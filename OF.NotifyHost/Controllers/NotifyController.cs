﻿using OF.Notify.DataHost;
using OF.Notify.Entity;
using OF.Notify.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;


namespace OF.NotifyHost.Controllers
{
    public class NotifyController : ApiController, INotifyService
    {
        [HttpPost]
        public CallServiceResult<List<TopicMessage>> GetMessages(GetMessageRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.GetMessages(request);
        }

        [HttpPost]
        public CallServiceResult<bool> AssignNodeDataCollection(AssignNodeDataCollectionRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.AssignNodeDataCollection(request);
        }

        [HttpPost]
        public CallServiceResult<bool> StopSync(StopSyncRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.StopSync(request);
        }

        [HttpPost]
        public CallServiceResult<bool> AddSyncNodeDataCollection(AddSyncNodeDataCollectionRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.AddSyncNodeDataCollection(request);
        }

        [HttpPost]
        public CallServiceResult<int> StoreMessage(SendMessageRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.StoreMessage(request);
        }

        [HttpPost]
        public CallServiceResult<bool> DeliverMessage(DeliverMessageRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.DeliverMessage(request);
        }

        [HttpPost]
        public CallServiceResult<int> SetDataCollectionReceiveless(SetDataCollectionReceivelessRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif
            return WebApiApplicationBase.dataNodeService.SetDataCollectionReceiveless(request);
        }

        [HttpPost]
        public CallServiceResult<List<int>> GetRestoreCollections(GetRestoreCollectionsRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif         
            return WebApiApplicationBase.dataNodeService.GetRestoreCollections(request);
        }

        [HttpPost]
        public CallServiceResult<bool> FinishedSync(FinishSyncRequest request)
        {
#if DEBUG
            request.ThrowExceptionIfMatch(MethodBase.GetCurrentMethod());
#endif    
            return WebApiApplicationBase.dataNodeService.FinishedSync(request);
        }


        [HttpPost] //[HttpGet]
        public CallServiceResult<string> DumpMessageData()
        {
            return CallServiceResult<string>.GetError(1, "message");
            //return CallServiceResult<string>.GetSuccess(WebApiApplicationBase.dataNodeService.DumpMessageData(tipMessage));
        }

    }
}
