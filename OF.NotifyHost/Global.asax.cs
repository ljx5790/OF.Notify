﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.DistributeService.Core.Common;
using OF.Notify.Client;
using OF.Notify.DataHost;
using OF.Notify.DataHost.Cluster;
using OF.Notify.DataHost.Cluster.Disk;
using OF.Notify.Test;
using OF.Notify.Test.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace OF.NotifyHost
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : WebApiApplicationBase
    {       
        protected void Application_Start()
        {
            InitConsumers();
            //MockContextConfig(ClusterContext.Get());
            base.OnApplicationStart();
        }

        private void InitConsumers()
        {
            Dictionary<byte, List<ConsumerBase>> topicConsumersDict = new Dictionary<byte, List<ConsumerBase>>();
            topicConsumersDict.Add((byte)TopicEnum.SO, new List<ConsumerBase> { 
                new MockConsumer(0, (byte)ConsumerEnum.Consumer1, 0),
                new MockConsumer(1, (byte)ConsumerEnum.Consumer2, 0)
            });
            var context = ClusterContext.Get();
            context.Init(topicConsumersDict);            
        }
        
        private void MockContextConfig(ClusterContext context)
        {
            //DiskCollectionOnlineMessageStore.EnableDeleteCollectionFile = false;
                /*
            context.SetMaxMessageCount(1);
            context.SetMaxInstantProcessCollectionCount(10);
            context.SetProcessCountOnce(10);
            context.SetStartupWaitProcessInterval(1);
            context.SetStartupWaitSyncInterval(1);
            context.SetSendMessageRetryCount(4);
                */
        }
        
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception objErr = Server.GetLastError().GetBaseException();
            Util.LogException("Application_Error", objErr);
        }

        protected void Application_End()
        {
            base.OnApplicationEnd();
        }
    }
}