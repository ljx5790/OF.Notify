﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using ConsoleApplication1.Cluster.Entity;
using OF.Notify.DataHost;
using OF.Notify.DataHost.Cluster.Entity;
using OF.Notify.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OF.Notify.Entity
{
    public class BaseRequest
    {
        public BaseRequest()
        { }

#if DEBUG
        public string[] throwExceptionCaseIds
        { get; set; }

        public void ThrowExceptionIfMatch(MethodBase methodInfo)
        {
            ThrowExceptionIfMatch(methodInfo.Name);
        }

        internal void ThrowExceptionIfMatch(string caseId)
        {
            if (throwExceptionCaseIds == null || throwExceptionCaseIds.Length == 0)
            {
                return;
            }
            if (throwExceptionCaseIds.Contains(caseId))
            {
                throw new Exception("ThrowExceptionIfMatch " + caseId);
            }
        }
#endif
    }

    public class DeliverMessageRequest : BaseRequest
    {
        public DeliverMessageRequest() { }

        public byte topicEnum
        { get; set; }

        public TopicMessage topicMessage
        { get; set; }
    }

    public class SendInstantCollectionFinishedRequest : BaseRequest
    {
        public SendInstantCollectionFinishedRequest() { }

        public int clusterId
        { get; set; }

        public int collectionId
        { get; set; }

        public byte topicEnum
        { get; set; }
            
        public byte consumerEnum
        { get; set; }

        public bool isSuccessed
        { get; set; }
    }

    public class GetProcessedTreeForRestoreRequest : BaseRequest
    {
        public GetProcessedTreeForRestoreRequest() { }

        public byte topicEnum
        { get; set; }
    }

    public class SendInstantProcessCollectionRequest : BaseRequest
    {
        public SendInstantProcessCollectionRequest() { }

        public byte topicEnum
        { get; set; }

        public byte consumerEnum
        { get; set; }

        public int collectionId
        { get; set; }
    }

    public class SendDisposeOnlineCollectionRequest : BaseRequest
    {
        public SendDisposeOnlineCollectionRequest() { }

        public int collectionId
        { get; set; }

        public int lastCollectionId
        { get; set; }

        public byte topicEnum
        { get; set; }
    }

    public class SendGetClusterNodeChangeToNotifyDataRequest : BaseRequest
    {
        public SendGetClusterNodeChangeToNotifyDataRequest() { }

        public byte topicEnum
        { get; set; }

        public List<KeyValuePair<int, List<SmallValue16AndFlag>>> clusterNodesList
        { get; set; }
    }


    public class SendSaveClusterNodeChangeToNotifyRequest : BaseRequest
    {
        public SendSaveClusterNodeChangeToNotifyRequest() { }

        public byte topicEnum
        { get; set; }

        public List<SmallValue16AndFlag> allClusterNodes
        { get; set; }

        public List<short> clusterOffLineNodes
        { get; set; }

        public int clusterId
        { get; set; }
    }

    public class SendGetClusterNodeChangeToNotifyCmdRequest : BaseRequest
    {
        public SendGetClusterNodeChangeToNotifyCmdRequest() { }

        public byte topicEnum
        { get; set; }

        public short nodeId
        { get; set; }
    }

    public class SendUpdateClusterNodesRequest : BaseRequest
    {
        public SendUpdateClusterNodesRequest() { }

        public byte topicEnum
        { get; set; }

        public List<SmallValue16AndFlag> allClusterNodes
        { get; set; }

        public int clusterId
        { get; set; }

        public List<short> syncClusterNodes
        { get; set; }
    }

    public class SendSyncCollectionCmdRequest : BaseRequest
    {
        public SendSyncCollectionCmdRequest() { }

        public byte topicEnum
        { get; set; }

        public int clusterId
        { get; set; }

        public List<short> nodeToSyncList
        { get; set; }
    }

    public class SendSyncCollectionDataRequest : BaseRequest
    {
        public SendSyncCollectionDataRequest() { }

        public byte topicEnum
        { get; set; }

        public int clusterId
        { get; set; }

        public short fromNodeId
        { get; set; }

        public List<DataCollectionMessages> collectionList
        { get; set; }
    }

    public class SendAssignOnLineClusterNodesRequest : BaseRequest
    {
        public SendAssignOnLineClusterNodesRequest() { }

        public byte topicEnum
        { get; set; }

        public List<short> toNodes
        { get; set; }

        public int onLineClusterId
        { get; set; }
    }

    public class SendGetUnProcessedFinishedCollectionsRequest : BaseRequest
    {
        public SendGetUnProcessedFinishedCollectionsRequest() { }

        public byte topicEnum
        { get; set; }

        public byte consumerEnum
        { get; set; }

        public List<int> unProcessedCollections
        { get; set; }
    }

    public class SendProcessCollectionsRequest : BaseRequest
    {
        public SendProcessCollectionsRequest() { }

        public byte topicEnum
        { get; set; }

        public byte consumerEnum
        { get; set; }

        public List<int> collections
        { get; set; }
    }

    public class SendSyncProcessedTreeRequest : BaseRequest
    {
        public SendSyncProcessedTreeRequest() { }

        public byte topicEnum
        { get; set; }

        public byte consumerEnum
        { get; set; }

        public IdTreeNode processedTreeNode
        { get; set; }

        public List<int> toDeleteCollections
        { get; set; }
    }

    public class SendAssignOnlineCollectionRequest : BaseRequest
    {
        public SendAssignOnlineCollectionRequest() { }

        public byte topicEnum
        { get; set; }

        public int clusterId
        { get; set; }

        public int collectionId
        { get; set; }
    }

    public class SendMessageRequest : BaseRequest
    {
        public SendMessageRequest() { }

        public byte topicEnum
        { get; set; }

        public int collectionId
        { get; set; }

        public TopicMessage message
        { get; set; }
    }

    public class GetAllRuntimeDataRequest : BaseRequest
    {
        public GetAllRuntimeDataRequest() { }

        public byte topicEnum
        { get; set; }

        public List<byte> consumerEnumList
        { get; set; }
    }

    public class SendGetUnProcessedFinishedCollectionsDTO
    {
        public SendGetUnProcessedFinishedCollectionsDTO() { }

        public List<int> finishedCollectionList
        { get; set; }

        public List<int> processingCollectionList
        { get; set; }

        public List<int> processedCollectionList
        { get; set; }
    }

    public class GetRuntimeDataRequest : BaseRequest
    {
        public byte topicEnum
        { get; set; }
    }
}