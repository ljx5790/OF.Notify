﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.Notify.Entity
{
    public class CallServiceResult<T>
    {
        public int ErrorCode = 0;
        public string ErrorMessage;
        public T Data;

        public CallServiceResult()
        { 
        
        }

        public bool IsSuccess()
        {
            return ErrorCode == 0;
        }

        public int GetErrorCode()
        {
            return ErrorCode;
        }

        public string GetErrorMessage()
        {
            return ErrorMessage;
        }

        public T GetData()
        {
            return Data;
        }

        public static CallServiceResult<T> GetSuccess(T data)
        {
            CallServiceResult<T> result = new CallServiceResult<T>();
            result.Data = data;
            return result;
        }

        public static CallServiceResult<T> GetError(int errorCode, string errorMessage)
        {
            CallServiceResult<T> result = new CallServiceResult<T>();
            if (errorCode == 0)
            {
                throw new ApplicationException("ErrorCode can't be 0!");
            }
            result.ErrorCode = errorCode;
            result.ErrorMessage = errorMessage;
            return result;
        }
    }
}
