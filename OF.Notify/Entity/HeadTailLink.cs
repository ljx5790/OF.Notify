﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.Notify.Entity
{
    public class HeadTailLink<T>
    {
        public bool IsFull = false;
        internal LinkItem<T> head;
        internal LinkItem<T> tail;
        public HeadTailLink()
        { 
        
        }

        public void Add(LinkItem<T> item)
        {
            lock (this)
            {
                if (head == null)
                {
                    head = item;
                    tail = item;
                }
                else
                {
                    tail.SetNext(item);
                    tail = item;
                }
            }
        }

        public LinkItem<T> GetHead()
        {
            return this.head;
        }

        public List<T> GetList()
        {
            lock (this)
            {
                List<T> result = new List<T>();
                LinkItem<T> current = head;
                while (current != null)
                {
                    result.Add(current.Data);
                    current = current.GetNext();
                }
                return result;
            }
        }

        public void ForEach(Action<T> action)
        {
            lock (this)
            {
                LinkItem<T> current = head;
                while (current != null)
                {
                    action(current.Data);
                    current = current.GetNext();
                }
            }
        }

        public LinkItem<T> GetNext(LinkItem<T> item)
        {
            lock (this)
            {
                if (item == null)
                {
                    return this.head;
                }
                else
                {
                    return item.GetNext();
                }
            }
        }
    }
}
