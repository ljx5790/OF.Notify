﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.Notify.Entity
{
    public class TopicMessage
    {
        public byte topicEnum
        { get; set; }

        public byte[] body
        { get; set; }

        public TopicMessage()
        { 
            
        }

        public TopicMessage(byte topicEnum, byte[] body)
        {
            this.topicEnum = topicEnum;
            this.body = body;
        }


        public byte[] GetContent()
        {
            return body;
        }

        public static TopicMessage Parse(byte topicEnum, byte[] body)
        {
            return new TopicMessage(topicEnum, body);
        }
    }
}