﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.Notify.DataHost.Cluster.Entity;
using OF.Notify.Entity;
using OF.Notify.Master;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OF.Notify.DataHost.Cluster;
using OF.Notify.Client;
using OF.Notify.Test.Entity;
using OF.Notify.DataHost.Cluster.Disk;
using OF.Notify.Channel;
using OF.Notify.Test;
using OF.DistributeService.Core.Common;

namespace OF.Notify.TestWeb
{
    public class TestWebHost
    {
        internal static Dictionary<byte, List<ConsumerBase>> topicConsumersDict = new Dictionary<byte, List<ConsumerBase>>();
        static TestWebHost()
        {
            topicConsumersDict.Add((byte)TopicEnum.SO, new List<ConsumerBase> { 
                new MockConsumer(0, (byte)ConsumerEnum.Consumer1, 0),
                new MockConsumer(1, (byte)ConsumerEnum.Consumer2, 0)
            });
            /*
            ClusterContext.enableCountApiCall = true;
            DiskCollectionOnlineMessageStore.EnableDeleteCollectionFile = false;
            */
        }

        public static void InitApp()
        {
            var context = ClusterContext.Get();
            context.Init(topicConsumersDict);
        }

        private static void DumpAll(string msg)
        { }

        internal static bool MyIsProcessed(int sleepInterval)
        {
            byte topicEnum = (byte)TopicEnum.SO;
            return WebAutoTestCaseManager.IsProcessed(topicEnum, topicConsumersDict[topicEnum], sleepInterval);
        }
        internal static bool MyMockSendMessage()
        {
            return WebNodeRemoting.MockSendMessage((byte)TopicEnum.SO);
        }

        internal static void MyTest(int nodeCount, int onlineNodeCount)
        {
            WebAutoTestCaseManager.Test(topicConsumersDict, nodeCount, onlineNodeCount);
        }

        public static void CheckPerfromance(int batchCount)
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                DateTime dtStart = DateTime.Now;
                for (int i1 = 0; i1 < batchCount; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }
                Util.LogInfo("sucessed message count is:" + totalMsgCount + ",send message elapse(ms):" + DateTime.Now.Subtract(dtStart).TotalMilliseconds);
                while (true)
                {
                    MyTest(4, 3);
                    bool isFinished = MyIsProcessed(1);
                    if (isFinished)
                    {
                        break;
                    }
                }
                Util.LogInfo("At end!consume message elapse(ms):" + DateTime.Now.Subtract(dtStart).TotalMilliseconds);
            }
            finally
            {
                WebNodeRemoting.DoDispose();
            }
        }

        public static void CheckSendMessages(int batchCount)
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (int i1 = 0; i1 < batchCount; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }
                DumpAll("1");
                MyTest(4, 4);
                Console.WriteLine("WebNodeRemoting.MockDisposeNode(2);");
                Console.Read();
                for (int i1 = 0; i1 < batchCount; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 2 i1:" + i1);
                    }
                }
                DumpAll("2");
                MyTest(4, 3);
                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                while(true)
                {
                    MyTest(4, 3);
                    bool isFinished = MyIsProcessed(1);
                    if (isFinished)
                    {
                        break;
                    }
                }
                Util.LogInfo("At end!");
            }
            finally
            {
                WebNodeRemoting.DoDispose();
            }
        }
    }
}