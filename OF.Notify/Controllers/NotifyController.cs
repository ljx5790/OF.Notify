﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


using ConsoleApplication1.Cluster.Entity;
using OF.DistributeService.Core.Common;
using OF.Notify.DataHost;
using OF.Notify.DataHost.Cluster.Entity;
using OF.Notify.Entity;
using OF.Notify.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace OF.Notify.Controllers
{
    public class NotifyController : ApiController, INotifyService
    {
        [HttpPost] 
        public CallServiceResult<bool> DeliverMessage(DeliverMessageRequest request)
        {
            //IncCallCount("DeliverMessage");
            try
            {
                return WebApiApplicationBase.dataNodeService.DeliverMessage(request);
            }
            catch (Exception ex)
            {
                Util.LogException("DeliverMessage", ex);
                return CallServiceResult<bool>.GetError(1, ex.ToString());
            }
        }

        [HttpPost] 
        public CallServiceResult<bool> SendMessage(SendMessageRequest request)
        {
            //IncCallCount("SendMessage");
            try
            {
                return WebApiApplicationBase.dataNodeService.SendMessage(request);
            }
            catch (Exception ex)
            {
                Util.LogException("SendSaveClusterNodeChangeToNotify", ex);
                return CallServiceResult<bool>.GetError(1, ex.ToString());
            }
        }

        [HttpPost] 
        public AllNodesRuntimeData GetAllRuntimeData(GetAllRuntimeDataRequest request)
        {
            return WebApiApplicationBase.dataNodeService.GetAllRuntimeData(request);
        }

        [HttpPost]
        public NodeRuntimeData GetRuntimeData(GetRuntimeDataRequest request)
        {
            return WebApiApplicationBase.dataNodeService.GetRuntimeData(request);
        }

        [HttpPost] 
        public CallServiceResult<bool> MockDisposeAllCluster()
        {
            try
            {
                return WebApiApplicationBase.dataNodeService.MockDisposeAllCluster();
            }
            catch (Exception ex)
            {
                Util.LogException("SendSaveClusterNodeChangeToNotify", ex);
                return CallServiceResult<bool>.GetError(1, ex.ToString());
            }
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendInstantCollectionFinished(SendInstantCollectionFinishedRequest request)
        {
            //IncCallCount("SendInstantCollectionFinished");
            return WebApiApplicationBase.dataNodeService.SendInstantCollectionFinished(request);            
        }

        [HttpPost] 
        public Task<CallServiceResult<Dictionary<byte, IdTreeNode>>> GetProcessedTreeForRestore(GetProcessedTreeForRestoreRequest request)
        {
            return WebApiApplicationBase.dataNodeService.GetProcessedTreeForRestore(request);            
        }


        [HttpPost] 
        public Task<CallServiceResult<bool>> SendInstantProcessCollection(SendInstantProcessCollectionRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendInstantProcessCollection(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendDisposeOnlineCollection(SendDisposeOnlineCollectionRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendDisposeOnlineCollection(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<List<KeyValuePair<int, List<SmallValue16AndFlag>>>>> SendGetClusterNodeChangeToNotifyData(SendGetClusterNodeChangeToNotifyDataRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendGetClusterNodeChangeToNotifyData(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendSaveClusterNodeChangeToNotify(SendSaveClusterNodeChangeToNotifyRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendSaveClusterNodeChangeToNotify(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendGetClusterNodeChangeToNotifyCmd(SendGetClusterNodeChangeToNotifyCmdRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendGetClusterNodeChangeToNotifyCmd(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<List<SmallValue16AndFlag>>> SendUpdateClusterNodes(SendUpdateClusterNodesRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendUpdateClusterNodes(request);
		}

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendSyncCollectionCmd(SendSyncCollectionCmdRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendSyncCollectionCmd(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendSyncCollectionData(SendSyncCollectionDataRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendSyncCollectionData(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendAssignOnLineClusterNodes(SendAssignOnLineClusterNodesRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendAssignOnLineClusterNodes(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<SendGetUnProcessedFinishedCollectionsDTO>> SendGetUnProcessedFinishedCollections(SendGetUnProcessedFinishedCollectionsRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendGetUnProcessedFinishedCollections(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<List<int>>> SendProcessCollections(SendProcessCollectionsRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendProcessCollections(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendSyncProcessedTree(SendSyncProcessedTreeRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendSyncProcessedTree(request);
        }

        [HttpPost] 
        public Task<CallServiceResult<bool>> SendAssignOnlineCollection(SendAssignOnlineCollectionRequest request)
        {
            return WebApiApplicationBase.dataNodeService.SendAssignOnlineCollection(request);
        }
    }
}
