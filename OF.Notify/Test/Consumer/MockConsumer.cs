﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.DistributeService.Core.Common;
using OF.Notify.Client;
using OF.Notify.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OF.Notify.Test
{
    public class MockConsumer : ConsumerBase
    {
        internal int id;
        internal byte consumerEnum;
        internal int sleepInterval;
        internal HashSet<int> hashSet = new HashSet<int>();
        public MockConsumer(int id, byte consumerEnum, int sleepInterval)
        { 
            this.id = id;
            this.consumerEnum = consumerEnum;
            this.sleepInterval = sleepInterval;
        }
         
        public override byte GetEnumType()
        {
            return this.consumerEnum;
        }

        public int GetMessageCount()
        {
            return hashSet.Distinct().Count();
        }

        public int GetId()
        {
            return id;
        }

        public override void OnMessage(bool isInstantMessage, TopicMessage message)
        {
            DateTime dtStart = DateTime.Now;
            if (sleepInterval > 0)
            {
                Thread.Sleep(sleepInterval);
            }
          
            int index = BitConverter.ToInt32(message.body, 0);
            if (!hashSet.Contains(index))
            {
                hashSet.Add(index);
            }
            Util.LogInfo("Consumer:" + id + ",Type:" + consumerEnum + ",message:" + index + ", isInstantMessage:" + isInstantMessage + ",hashSetcount:" + hashSet.Count);
        }
    }
}
