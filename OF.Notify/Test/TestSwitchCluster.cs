﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.DistributeService.Core.Common;
using OF.Notify.Client;
using OF.Notify.DataHost.Cluster;
using OF.Notify.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OF.Notify.Test.Entity;

namespace OF.Notify.Test
{
    public class TestSwitchCluster
    {
        internal static Dictionary<byte, List<ConsumerBase>> topicConsumersDict = new Dictionary<byte, List<ConsumerBase>>();
        static TestSwitchCluster()
        {
            topicConsumersDict.Add((byte)TopicEnum.SO, new List<ConsumerBase> { 
                new MockConsumer(0, (byte)ConsumerEnum.Consumer1, 100),
                new MockConsumer(1, (byte)ConsumerEnum.Consumer2, 400)
            }); 
        }


        internal static void DumpAll(string str)
        {
            AutoTestCaseManager.DumpAll((byte)TopicEnum.SO, str);
        }

        internal static void MyIsProcessed(int sleepInterval)
        {
            byte topicEnum = (byte)TopicEnum.SO;
            AutoTestCaseManager.IsProcessed(topicEnum, topicConsumersDict[topicEnum], sleepInterval);
        }
        internal static bool MyMockSendMessage()
        {
            return NodeRemoting.MockSendMessage((byte)TopicEnum.SO);
        }

        internal static void MyTest(int nodeCount, int onlineNodeCount)
        {
            AutoTestCaseManager.Test(topicConsumersDict, nodeCount, onlineNodeCount);
        }

        public static void InitApp()
        {
            var context = ClusterContext.Get();
            context.SetMaxMessageCount(1);
            context.SetMaxInstantProcessCollectionCount(10);
            context.SetProcessCountOnce(10);
            context.SetStartupWaitProcessInterval(1);
            context.SetStartupWaitSyncInterval(1);
            context.SetSendMessageRetryCount(4);
            
            ClusterContext.Get().Init(topicConsumersDict);
        }

        public static void TestSwitch()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                ClusterContext.Get().SetMaxClusterCollectionCount(3);
                for (short i1 = 1; i1 <= 2; i1++)
                {
                    NodeRemoting.MockNewNode(i1, true);
                }
               
                for (int i1 = 0; i1 < 10; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }
                DumpAll("1");
                MyTest(2, 2);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    MyIsProcessed(5);
                }
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }
    }
}
