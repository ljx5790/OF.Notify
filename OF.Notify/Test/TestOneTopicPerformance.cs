﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.DistributeService.Core.Common;
using OF.Notify.DataHost.Cluster.Entity;
using OF.Notify.Entity;
using OF.Notify.Master;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OF.Notify.DataHost.Cluster;
using OF.Notify.Client;
using OF.Notify.Test.Entity;

namespace OF.Notify.Test
{
    public class TestOneTopicPerformance
    {
        internal static Dictionary<byte, List<ConsumerBase>> topicConsumersDict = new Dictionary<byte, List<ConsumerBase>>();
        static TestOneTopicPerformance()
        {
            topicConsumersDict.Add((byte)TopicEnum.SO, new List<ConsumerBase> { 
                new MockConsumer(0, (byte)ConsumerEnum.Consumer1, 100),
                new MockConsumer(1, (byte)ConsumerEnum.Consumer2, 400)
            }); 
        }

        public static void CheckSendMessages(int batchCount)
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1, true);
                }
                for (int i1 = 0; i1 < batchCount; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }
                DumpAll("1");
                MyTest(4, 4);
                NodeRemoting.MockDisposeNode(2);
                for (int i1 = 0; i1 < batchCount; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 2 i1:" + i1);
                    }
                }
                DumpAll("2");
                MyTest(4, 3);
                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                for (int i1 = 0; i1 < 100; i1++)
                {
                    MyTest(4, 3);
                    MyIsProcessed(5);
                }
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }

        public static void TestPerformance()
        {
            InitApp();
            int totalMsgCount = 0;
            int totalLoopCount = 1024 * 1024;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1, true);
                }
                DateTime dtStart = DateTime.Now;
                Util.LogInfo("Begin:" + dtStart.ToString("HH:mm:ss.fff"));
                for (int i1 = 0; i1 < totalLoopCount; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }
                //MyIsProcessed(5); //small can view processed!
                DateTime dtEnd = DateTime.Now;
                Util.LogInfo("End:" + dtEnd.ToString("HH:mm:ss.fff") + ",TotalSeconds:" + dtEnd.Subtract(dtStart).TotalSeconds + " S");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }

        internal static void DumpAll(string str)
        {
            AutoTestCaseManager.DumpAll((byte)TopicEnum.SO, str);
        }

        internal static void MyIsProcessed(int sleepInterval)
        {
            byte topicEnum = (byte)TopicEnum.SO;
            AutoTestCaseManager.IsProcessed(topicEnum, topicConsumersDict[topicEnum], sleepInterval);
        }
        internal static bool MyMockSendMessage()
        {
            return NodeRemoting.MockSendMessage((byte)TopicEnum.SO);
        }

        internal static void MyTest(int nodeCount, int onlineNodeCount)
        {
            AutoTestCaseManager.Test(topicConsumersDict, nodeCount, onlineNodeCount);
        }

        public static void InitApp()
        {
            var context = ClusterContext.Get();
            context.Init(topicConsumersDict);
        }

        public static void CheckClusterCrash6()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                NodeRemoting.MockNewNode(5);
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1);
                }


                NodeRemoting.MockNewNode(6);

                NodeRemoting.MockNewNode(7);

                NodeRemoting.Start();
                Thread.Sleep(2000);

                DumpAll("6");
                MyTest(7, 7);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    MyIsProcessed(5);
                }
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }

        public static void CheckClusterCrash5()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1);
                }
                NodeRemoting.MockNewNode(5);

                NodeRemoting.MockDisposeNode(5);
                NodeRemoting.MockNewNode(6);
                NodeRemoting.MockDisposeNode(6);

                NodeRemoting.MockNewNode(7);
                NodeRemoting.MockDisposeNode(7);

                NodeRemoting.MockDisposeNode(2);
                NodeRemoting.MockNewNode(5);
                NodeRemoting.MockDisposeNode(1);

                NodeRemoting.MockDisposeNode(3);

                NodeRemoting.Start();
                Thread.Sleep(2000);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 5 i1:" + i1);
                    }
                }
                //DumpAll("5");
                DumpAll("5");
                MyTest(7, 2);

                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }

        public static void CheckClusterCrash4()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1);
                }
                NodeRemoting.MockNewNode(5);

                NodeRemoting.MockDisposeNode(5);
                NodeRemoting.MockNewNode(6);
                NodeRemoting.MockDisposeNode(6);

                NodeRemoting.MockNewNode(7);
                NodeRemoting.MockDisposeNode(7);

                NodeRemoting.MockDisposeNode(2);
                NodeRemoting.MockNewNode(5);

                NodeRemoting.MockDisposeNode(1);
                NodeRemoting.Start();
                Thread.Sleep(2000);
                
                DumpAll("4");
                MyTest(7, 3);
                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }

        public static void CheckClusterCrash3()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1);
                }
                NodeRemoting.MockNewNode(5);

                NodeRemoting.MockDisposeNode(5);
                NodeRemoting.MockNewNode(6);
                NodeRemoting.MockDisposeNode(6);

                NodeRemoting.MockNewNode(7);
                NodeRemoting.MockDisposeNode(7);

                NodeRemoting.MockDisposeNode(2);

                NodeRemoting.MockNewNode(5);

                NodeRemoting.Start();
                Thread.Sleep(2000);
                for (int i1 = 0; i1 < 8; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 3 i1:" + i1);
                    }
                }
                //DumpAll("3");
                DumpAll("3");
                MyTest(7, 4);

                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }


        public static void CheckClusterCrash2()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1);
                }

                NodeRemoting.MockNewNode(5);

                NodeRemoting.MockDisposeNode(5);
                NodeRemoting.MockNewNode(6);
                NodeRemoting.MockDisposeNode(6);

                NodeRemoting.MockNewNode(7);
                NodeRemoting.MockDisposeNode(7);

                NodeRemoting.MockDisposeNode(2);

                Util.LogInfo("before start");

                NodeRemoting.Start();
                Thread.Sleep(2000);

                for (int i1 = 0; i1 < 10; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 2 i1:" + i1);
                    }
                }

                //DumpAll("2");
                Util.LogInfo("before AutoTestCaseManager.Test");
                DumpAll("2");
                MyTest(7, 3);

                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                //MyIsProcessed(5, AutoTestCaseManager.GetFromEnd(0, totalMsgCount - 1 - 1));

                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }

        public static void CheckClusterCrash1()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1);
                }
                NodeRemoting.MockNewNode(5);

                NodeRemoting.MockDisposeNode(5);
                NodeRemoting.MockNewNode(6);
                NodeRemoting.MockDisposeNode(6);

                NodeRemoting.MockNewNode(7);
                NodeRemoting.MockDisposeNode(7);

                NodeRemoting.Start();

                for (int i1 = 0; i1 < 8; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }

                //DumpAll("1");				
                DumpAll("1");
                MyTest(7, 4);



                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                //MyIsProcessed(5, AutoTestCaseManager.GetFromEnd(0, totalMsgCount - 1 - 1));

                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }



        public static void SendMessageWithClusterChange()
        {
            InitApp();
            int totalMsgCount = 0;
            try
            {
                for (short i1 = 1; i1 <= 4; i1++)
                {
                    NodeRemoting.MockNewNode(i1, true);
                }
                NodeRemoting.MockNewNode(5, true);

                NodeRemoting.MockDisposeNode(5);
                NodeRemoting.MockNewNode(6, true);
                NodeRemoting.MockDisposeNode(6);

                NodeRemoting.MockNewNode(7, true);
                NodeRemoting.MockDisposeNode(7);


                for (int i1 = 0; i1 < 8; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 1 i1:" + i1);
                    }
                }
                DumpAll("1");
                MyTest(7, 4);
                NodeRemoting.MockDisposeNode(2);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 2 i1:" + i1);
                    }
                }
                DumpAll("2");
                MyTest(7, 3);
                NodeRemoting.MockNewNode(5, true);

                for (int i1 = 0; i1 < 8; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 3 i1:" + i1);
                    }
                }
                DumpAll("3");
                MyTest(7, 4);
                NodeRemoting.MockDisposeNode(1);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 4 i1:" + i1);
                    }
                }
                DumpAll("4");
                MyTest(7, 3);
                NodeRemoting.MockDisposeNode(3);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    if (MyMockSendMessage())
                    {
                        totalMsgCount++;
                    }
                    else
                    {
                        Util.LogInfo("...........send message failed at 5 i1:" + i1);
                    }
                }
                DumpAll("5");
                MyTest(7, 2);

                Util.LogInfo("sucessed message count is:" + totalMsgCount);
                for (int i1 = 0; i1 < 10; i1++)
                {
                    MyIsProcessed(5);
                }
                Util.LogInfo("At end!");
            }
            finally
            {
                NodeRemoting.DoDispose();
            }
        }
         
    }
}

 

