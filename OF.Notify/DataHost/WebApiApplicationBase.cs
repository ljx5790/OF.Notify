﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.DistributeService.Core.Client;
using OF.DistributeService.Core.Common;
using OF.DistributeService.Core.Entity;
using System;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using OF.Notify.DataHost;
using OF.Notify.Entity;
using OF.Notify.DataHost.Cluster;
using System.Collections.Generic;
using OF.Notify.Client;
using OF.Notify.Test;
using OF.Notify.Test.Entity;
using OF.Notify.Channel;

namespace OF.Notify.DataHost
{
    public class WebApiApplicationBase : System.Web.HttpApplication
    {
        public static DataNode dataNodeService = null;

        protected void OnApplicationStart()
        {
            WebApiConfig.Register(GlobalConfiguration.Configuration);

            AppDomain.CurrentDomain.DomainUnload += CurrentDomain_DomainUnload;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += (sender, args) => Util.LogException("............UnobservedTaskException", args.Exception);
            
            ClusterContext.SetNotifyServiceProvider(new NotifyServiceProvider());
            dataNodeService = new DataNode(DataNodeConfig.GetFromConfigFile());
            dataNodeService.Start();
        }

        internal void Dispose()
        {
            if (dataNodeService != null)
            {
                dataNodeService.DoDispose();
                dataNodeService = null;
            }
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Util.LogException("............CurrentDomain_UnhandledException", e.ExceptionObject as Exception);
        }

        void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            Util.LogInfo("CurrentDomain_DomainUnload!");
            Dispose();
        }

        protected void OnApplicationEnd()
        {
            Util.LogInfo("OnApplicationEnd!");
            Dispose();
        }
    }
}
