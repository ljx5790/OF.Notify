﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.Notify.DataHost.Cluster.Disk
{
    public class DiskIdToBigValuesMapper : IIdToBigValuesMapper
    {
        internal static int TempId = 0;
        internal const int MaxBufferSize = 1024 * 1024 * 100;

        internal Func<int, string> getIdPathFunc = null;
        internal int combineRate = 32768;
        internal TopicClusterContext topicContext = null;
        public DiskIdToBigValuesMapper(TopicClusterContext topicContext, Func<int, string> getIdPathFunc)
        {
            this.topicContext = topicContext;
            this.getIdPathFunc = getIdPathFunc;
        }

        public void AppendValue(int id, int value)
        {
            string idPath = getIdPathFunc(id);
            ClusterContext context = ClusterContext.Get();
            string valuePath = topicContext.GetIdBigValuePath(idPath, combineRate, value);
            Int16 offSet = (Int16)(value % combineRate);
            byte[] btArray = BitConverter.GetBytes(offSet);
            DiskVisitor.EnsureFileDirectoryExists(valuePath);
            ClusterContext.StringLock.Invoke(valuePath, () => {
                using (FileStream fs = new FileStream(valuePath, FileMode.Append))
                {
                    fs.Write(btArray, 0, btArray.Length);
                }  
            });
        }

        internal void WriteValuePath(string valuePath, IEnumerable<int> group)
        {
            DiskVisitor.EnsureFileDirectoryExists(valuePath);
            ClusterContext.StringLock.Invoke(valuePath, () => {
                using (FileStream fs = new FileStream(valuePath, FileMode.Append))
                {
                    foreach (var value in group)
                    {
                        Int16 offSet = (Int16)(value % combineRate);
                        byte[] btArray = BitConverter.GetBytes(offSet);
                        fs.Write(btArray, 0, btArray.Length);
                    }
                }
            });
        }

        public void AppendValue(int id, List<int> values)
        {
            string idPath = getIdPathFunc(id);
            foreach (var group in values.GroupBy(v => v / combineRate))
            {
                int sameGroupValue = group.Key * combineRate;
                string valuePath = topicContext.GetIdBigValuePath(idPath, combineRate, sameGroupValue);
                WriteValuePath(valuePath, group);
            }
        }

        internal List<int> GetValues(string filePath)
        {
            byte[] content = GetFileContent(filePath);
            if (content == null || content.Length <= 0)
            {
                return null;
            }
            string fileName = filePath.Substring(filePath.LastIndexOf("\\") + 1);
            if (fileName.IndexOf("_") >= 0)
            {
                return null;
            }
            int baseValue = int.Parse(fileName) * combineRate;
            List<int> result = new List<int>(content.Length / 2);
            for (int loopI = 0; loopI < content.Length; loopI += 2)
            {
                result.Add(baseValue + BitConverter.ToInt16(content, loopI));
            }
            return result;
        }

        internal byte[] GetFileContent(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }
            byte[] btArray = null;
            ClusterContext.StringLock.Invoke(filePath, () => {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    if (fs.Length > MaxBufferSize)
                    {
                        throw new NotSupportedException();
                    }
                    btArray = new byte[fs.Length];
                    fs.Read(btArray, 0, btArray.Length);
                }
            });
            return btArray;
        }

        public List<int> GetTop(int id)
        {
            string idPath = getIdPathFunc(id);
            ClusterContext context = ClusterContext.Get();
            List<int> result = null;
            DiskVisitor.SequenceVisitFiles(idPath, (filePath) => {
                string fileName = filePath.Substring(filePath.LastIndexOf("\\") + 1);
                if (fileName.IndexOf("_") >= 0)
                {
                    return false;
                }
                result = GetValues(filePath);
                return true;
            });
            return result;
        }

        public void VisitAll(int id, Func<List<int>, bool> func)
        {
            string idPath = getIdPathFunc(id);
            ClusterContext context = ClusterContext.Get();
            DiskVisitor.SequenceVisitFiles(idPath, (filePath) =>
            {
                string fileName = filePath.Substring(filePath.LastIndexOf("\\") + 1);
                if (fileName.IndexOf("_") < 0)
                {
                    List<int> values = GetValues(filePath);
                    if (values != null && values.Count > 0)
                    {
                        values = values.Distinct().ToList();
                        bool isEnd = func(values);
                        if (isEnd)
                        {
                            return true;
                        }
                    }
                }
                return false;
            });
        }
    }

}
