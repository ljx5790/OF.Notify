﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Cluster.Entity
{
    public class SmallValue16AndFlag
    {
        public const int CellLen = 2;

        public Int16 Value;
        public SmallValue16AndFlagStatus Status;
        internal const int MultiRate = 3;
        public byte[] GetBytes()
        {
            int temp = GetCombineValue();
            return BitConverter.GetBytes((Int16)temp);
        }

        public static SmallValue16AndFlag Parse(byte[] btArray, int startIndex)
        {
            int temp = (int)BitConverter.ToInt16(btArray, startIndex);
            SmallValue16AndFlag result = new SmallValue16AndFlag();
            result.Status = (SmallValue16AndFlagStatus)((byte)(temp % MultiRate));
            result.Value = (Int16)(temp / MultiRate);
            return result;
        }

        public int GetCombineValue()
        { 
            int temp = Value * MultiRate;
            temp += (int)Status;
            return temp;
        }

        public int GetOrderValue()
        {
            int temp = Value * MultiRate;
            if (Status == SmallValue16AndFlagStatus.Synced)
            {
                temp += (int)Status;
            }
            return temp;
        }
    }

    public enum SmallValue16AndFlagStatus : byte
    { 
        Syncing = 0,
        Deleted = 1,
        Synced = 2
    }
}
