﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using ConsoleApplication1.Cluster.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OF.Notify.DataHost.Cluster.Disk;

namespace OF.Notify.DataHost.Cluster
{
    public interface IIdToSmallValuesMapper
    {
        void AppendValue(int id, SmallValue16AndFlag value);
        void AppendValue(int id, List<SmallValue16AndFlag> values);
        void ResetValues(int id, List<SmallValue16AndFlag> values);
        List<SmallValue16AndFlag> GetValues(int id);
        bool Exists(int id);
        void TryRemove(List<int> keyList);
        void VisitAll(int batchSize, Action<List<KeyValuePair<int, List<SmallValue16AndFlag>>>> action);
    }
}
