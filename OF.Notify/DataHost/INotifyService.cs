﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com) & Quincy.

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using ConsoleApplication1.Cluster.Entity;
using OF.Notify.DataHost.Cluster.Entity;
using OF.Notify.Entity;
using OF.Notify.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.Notify.DataHost
{
    public interface INotifyService
    {        
        CallServiceResult<bool> DeliverMessage(DeliverMessageRequest request);
        CallServiceResult<bool> SendMessage(SendMessageRequest request);
        AllNodesRuntimeData GetAllRuntimeData(GetAllRuntimeDataRequest request);
        CallServiceResult<bool> MockDisposeAllCluster();

        Task<CallServiceResult<bool>> SendInstantCollectionFinished(SendInstantCollectionFinishedRequest request);
        Task<CallServiceResult<Dictionary<byte, IdTreeNode>>> GetProcessedTreeForRestore(GetProcessedTreeForRestoreRequest request);
        Task<CallServiceResult<bool>> SendInstantProcessCollection(SendInstantProcessCollectionRequest request);
        Task<CallServiceResult<bool>> SendDisposeOnlineCollection(SendDisposeOnlineCollectionRequest request);
        Task<CallServiceResult<List<KeyValuePair<int, List<SmallValue16AndFlag>>>>> SendGetClusterNodeChangeToNotifyData(SendGetClusterNodeChangeToNotifyDataRequest request);
        Task<CallServiceResult<bool>> SendSaveClusterNodeChangeToNotify(SendSaveClusterNodeChangeToNotifyRequest request);
        Task<CallServiceResult<bool>> SendGetClusterNodeChangeToNotifyCmd(SendGetClusterNodeChangeToNotifyCmdRequest request);
        Task<CallServiceResult<List<SmallValue16AndFlag>>> SendUpdateClusterNodes(SendUpdateClusterNodesRequest request);
        Task<CallServiceResult<bool>> SendSyncCollectionCmd(SendSyncCollectionCmdRequest request);
        Task<CallServiceResult<bool>> SendSyncCollectionData(SendSyncCollectionDataRequest request);
        Task<CallServiceResult<bool>> SendAssignOnLineClusterNodes(SendAssignOnLineClusterNodesRequest request);
        Task<CallServiceResult<SendGetUnProcessedFinishedCollectionsDTO>> SendGetUnProcessedFinishedCollections(SendGetUnProcessedFinishedCollectionsRequest request);
        Task<CallServiceResult<List<int>>> SendProcessCollections(SendProcessCollectionsRequest request);
        Task<CallServiceResult<bool>> SendSyncProcessedTree(SendSyncProcessedTreeRequest request);
        Task<CallServiceResult<bool>> SendAssignOnlineCollection(SendAssignOnlineCollectionRequest request);
        NodeRuntimeData GetRuntimeData(GetRuntimeDataRequest request);
    }
}
